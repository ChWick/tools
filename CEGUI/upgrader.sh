FILES=${PWD}/*


for file in $FILES
do
    if [ -w "$file" ]
    then
	echo "Processing ${file}"
    else
	echo "No writing permission on ${file}"
        continue
    fi
    
    sed -e 's/(Point(\([0-9/.f]*\), \([0-9/.f]*\)))/(UVector2(UDim(\1, 0), UDim(\2, 0)))/g' \
	-e 's/(Size(\([0-9/.f]*\), \([0-9/.f]*\)))/(USize(UDim(\1, 0), UDim(\2, 0)))/g' \
	-e 's/setMaximumSize/setMaxSize/g' \
	-e 's/setMinimumSize/setMinSize/g' \
	-e 's/ms_Singleton/msSingleton/g' \
	-e 's/StaticText\*/Window\*/g' \
	-e 's/setBackgroundEnabled(\([a-z]\+\))/setProperty("BackgroundEnabled", "\1")/g' \
	-e 's/setFrameEnabled(\([a-z]\+\))/setProperty("FrameEnabled", "\1")/g' \
	-e 's/Vector3(/Vector3f(/g' \
	-e 's/setWindowPosition/setPosition/g' \
	-e 's/setWindowSize/setSize/g' \
	-e 's/setSize(UVector2(/setSize(USize(/g' \
	-e 's/ImagesetManager::getSingleton().getImageset("\([a-zA-Z_0-9]\+\)")->getImage("/ImageManager::getSingleton().get("\1\//g' \
	$file > tmp.txt
    mv tmp.txt $file

    perl -0777 -pe 's/WindowManager::getSingleton\(\)\.createWindow\((.*)\);[\t\n ]*([a-zA-Z_]+)->addChildWindow\([a-zA-Z_]*\);/\2->createChild\(\1\);/g ' $file > tmp.txt
    
    mv tmp.txt $file
    
done

